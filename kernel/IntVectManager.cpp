#include "IntVectManager.hpp"

using namespace kernel;

IntVectManager::IntVectManager()
{
	for (int i = 0; i < VECTOR_TABLE_SIZE; i++)
	{
		m_vectorTable[i] = (IrqHandler) &IntVectManager::defaultIsrHandler;
	}
}


IntVectManager::~IntVectManager()
{
	
}

void IntVectManager::defaultIsrHandler()
{
	asm("bkpt 0");
}

void IntVectManager::registerHandler(IRQn_Type irq, IrqHandler handler)
{
	m_vectorTable[((int16_t)irq) + 16] = handler;
}

void IntVectManager::registerHandler(uint16_t irq, IrqHandler handler)
{
	m_vectorTable[irq] = handler;
}

void IntVectManager::unregisterHandler(IRQn_Type irq)
{
	m_vectorTable[((int16_t)irq) + 16] = (IrqHandler) &IntVectManager::defaultIsrHandler;
}

uint32_t IntVectManager::tableBaseAddress()
{
	uint32_t baseAddress = (uint32_t)&m_vectorTable;
	return baseAddress;
}

IntVectManager::IrqHandler IntVectManager::getIsr(uint32_t isrNumber)
{
	return m_vectorTable[isrNumber];
}

uint8_t IntVectManager::s_numberOfSubBits;
uint8_t IntVectManager::s_numberOfPreEmptBits;


