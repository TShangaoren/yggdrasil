#include "Kernel.hpp"


using namespace kernel;

IntVectManager Scheduler::s_vectorTable = IntVectManager();
#ifdef SYSVIEW
IntVectManager Scheduler::s_sysviewVectorTable = IntVectManager();
#endif


bool Scheduler::s_schedulerStarted = false;
volatile uint64_t Scheduler::s_ticks = 0;
		
Task* Scheduler::s_activeTask = nullptr;
interface::ISystem* Scheduler::s_systemInterface = nullptr;
uint32_t Scheduler::s_sysTickFreq = 1000;

StartedList Scheduler::s_started;
ReadyList Scheduler::s_ready;
SleepingList Scheduler::s_sleeping;

TaskWithStack<128> Scheduler::s_idle = TaskWithStack<128>(idleTaskFunction, 0,"Idle");

