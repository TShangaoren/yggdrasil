#pragma once
#include <cstdint>

namespace kernel
{
	struct hwStackFrame
	{
		uint32_t r0;
		uint32_t r1;
		uint32_t r2;
		uint32_t r3;
			
		uint32_t r12;  //scratch register
		uint32_t retAdrr;  //return address
		uint32_t lr;  //link register r14
			
		uint32_t psr;
	};
		
	struct swStackFrame
	{
		uint32_t r4;
		uint32_t r5;
		uint32_t r6;
		uint32_t r7;
		uint32_t r8;
		uint32_t r9;
		uint32_t r10;
		uint32_t r11;
		uint32_t sp;  //stack pointer r13
		uint32_t pc;  //program counter r15
	};
		
	struct hwFpuStackFrame
	{
		uint32_t s0;
		uint32_t s1;
		uint32_t s2;
		uint32_t s3;
		uint32_t s4;
		uint32_t s5;
		uint32_t s6;
		uint32_t s7;
		uint32_t s8;
		uint32_t s9;
		uint32_t s10;
		uint32_t s11;
		uint32_t s12;
		uint32_t s13;
		uint32_t s14;
		uint32_t s15;
		uint32_t fpcr;
	};
		
	struct swFpuStackFrame
	{
		uint32_t s15;
		uint32_t s16;
		uint32_t s17;
		uint32_t s18;
		uint32_t s19;
		uint32_t s20;
		uint32_t s21;
		uint32_t s22;
		uint32_t s23;
		uint32_t s24;
		uint32_t s25;
		uint32_t s26;
		uint32_t s27;
		uint32_t s28;
		uint32_t s29;
		uint32_t s30;
		uint32_t s31;
	};
	
	//template<uint32_t stackSize>
	class Stack
	{
		
	public :  

		
	private:
		
		uint8_t* memory;
		uint32_t StackSize;
	};
}
