#pragma once

#include <cstdint>
#include "Kernel.hpp"
#include "ServiceCall.hpp"
#include "IntVectManager.hpp"
#include "Event.hpp"

#include "../interfaces/ISystem.hpp"


namespace kernel
{
	class Api
	{
	public:
		static inline bool startKernel(interface::ISystem& systemClock, uint8_t systemInterruptPriority, uint8_t numberOfSubPriorityBits)
		{
			return Scheduler::startKernel(systemClock,systemInterruptPriority,numberOfSubPriorityBits);
		}
		
		
		/*wait without using kernel*/
		static void wait(uint32_t ms)
		{
			uint64_t endWaitTimeStamp = Scheduler::s_ticks + ms;
			while (Scheduler::s_ticks <= endWaitTimeStamp) ;
		}
		
		static void __attribute__((aligned(4))) sleep(uint32_t ms)
		{
			asm volatile(
				"MOV R0,%0\n\t"
				"SVC %[immediate]"::"r" (ms), [immediate] "I"(ServiceCall::SvcNumber::sleepTask));
		}
		
		
		/*get kernel timeStamp*/
		static uint64_t getTicks()
		{
			return Scheduler::s_ticks;
		}
		
		/*register an irq before the scheduler has started*/
		static void registerIrq(IRQn_Type irq, IntVectManager::IrqHandler handler)
		{
			if (Scheduler::s_schedulerStarted)
				registerIrqKernel(irq, handler);
			else
				Scheduler::preSchedulerIrqRegister(irq, handler);
		}
		
		
		/*unregister an irq before the scheduler has started*/
		static void unregisterIrq(IRQn_Type irq)
		{
			if (Scheduler::s_schedulerStarted)
				unRegisterIrqKernel(irq);
			else
				Scheduler::preSchedulerIrqUnregister(irq);
		}
		

		/*Setup an Irq Priority*/
		static inline void irqPriority(IRQn_Type irq, uint8_t preEmpt, uint8_t sub)
		{
			if(Scheduler::s_schedulerStarted)
				irqPriorityKernel(irq,preEmpt,sub);
			else
				IntVectManager::irqPriority(irq,preEmpt,sub);
		}

		static inline void irqPriority(IRQn_Type irq, uint8_t priority)
		{
			if(Scheduler::s_schedulerStarted)
				irqGlobalPriorityKernel(irq,priority);
			else
				IntVectManager::irqPriority(irq,priority);
		}

		/*Enable an Irq in NVIC*/
		static inline void enableIrq(IRQn_Type irq)
		{
			if(Scheduler::s_schedulerStarted)
			{
				asm volatile(
					"MOV R0,%0\n\t"
					"SVC %[immediate]"::"r" (irq), [immediate] "I"(ServiceCall::SvcNumber::enableIrq));
			}
			else
				IntVectManager::enableIrq(irq);
		}

		/*Disable an Irq in NVIC*/
		static inline void disableIrq(IRQn_Type irq)
		{
			if(Scheduler::s_schedulerStarted)
			{
				asm volatile(
					"MOV R0,%0\n\t"
					"SVC %[immediate]"::"r" (irq), [immediate] "I"(ServiceCall::SvcNumber::disableIrq));
			}
			else
				IntVectManager::disableIrq(irq);
		}

		static inline void clearIrq(IRQn_Type irq)
		{
			if(Scheduler::s_schedulerStarted)
			{
				asm volatile(
					"MOV R0,%0\n\t"
					"SVC %[immediate]"::"r" (irq), [immediate] "I"(ServiceCall::SvcNumber::clearIrq));
			}
			else
				IntVectManager::clearIrq(irq);
		}

	private:
		/*All Svc function are naked to keep Register with value stored when called*/
		static bool __attribute__((naked)) registerIrqKernel(IRQn_Type irq, IntVectManager::IrqHandler handler)
		{
			asm volatile("PUSH {LR}");
			svc(ServiceCall::SvcNumber::registerIrq);
			asm volatile(
				"POP {LR}\n\t"
				"BX LR");
		}
		
		
		static bool __attribute__((naked)) unRegisterIrqKernel(IRQn_Type irq)
		{
			asm volatile("PUSH {LR}");
			svc(ServiceCall::SvcNumber::unregisterIrq);
			asm volatile(
				"POP {LR}\n\t"
				"BX LR");
		}
		
		static void __attribute__((naked)) irqGlobalPriorityKernel(IRQn_Type irq, uint8_t priority)
		{
			asm volatile("PUSH {LR}");
			svc(ServiceCall::SvcNumber::setGlobalPriority);
			asm volatile(
				"POP {LR}\n\t"
				"BX LR");
		}

		static void __attribute__((naked)) irqPriorityKernel(IRQn_Type irq, uint8_t preEmpt, uint8_t sub)
		{
			asm volatile("PUSH {LR}");
			svc(ServiceCall::SvcNumber::setPriority);
			asm volatile(
				"POP {LR}\n\t"
				"BX LR");
		}

	};
	}
