#pragma once

#include <cstdint>

#include "Task.hpp" 
#include "ServiceCall.hpp"


namespace kernel
{
	
	class Event
	{
		friend class Scheduler; //let Scheduler access private function but no one else
	public:
		
		Event(bool isRaised = false) : m_isRaised(isRaised)
		{
		}
		
		
		~Event()
		{
			
		}
		
		
				
		//wait for an event, used by Scheduler
		//first task to wait is first task to be served
		//@parameter Task waiting for the event
		//return true if the task is waiting,
		//false if event is already rised
		virtual bool wait()
		{
			waitEventKernel(this);
			return true;
		}
		
		
		//Signal that an event occured
		//if a task is already waiting then return it to wake it up
		//else rise event and return nullptr
		virtual bool signal()
		{
			signalEventKernel(this);
			return true;
		}
		
		virtual bool someoneWaiting()
		{
			if (!m_list.isEmpty())
				return true;
			else
				return false;
		}
		private:
		
		//------------------PRIVATE DATA------------------------
		EventList m_list;
		bool m_isRaised;
		
		
		
		
		//------------------PRIVATE FUNCTION---------------------
		static bool __attribute__((naked)) waitEventKernel(Event* event)
		{
			asm volatile("PUSH {LR}");
			svc(ServiceCall::SvcNumber::waitEvent);
			asm volatile(
				"POP {LR}\n\t"
				"BX LR");
		}
		
		static bool __attribute__((naked)) signalEventKernel(Event* event)
		{
			asm volatile("PUSH {LR}");
			svc(ServiceCall::SvcNumber::signalEvent);
			asm volatile(
				"POP {LR}\n\t"
				"BX LR");
		}
	
		};
	

}

