/*
 * Task.cpp
 *
 *  Created on: 27 sept. 2018
 *      Author: Flo
 */
#include "Kernel.hpp"

namespace kernel
{

		bool Task::start()
		{
			if(Scheduler::s_schedulerStarted)
				return startTaskStub(this);
			else
				return Scheduler::preSchedulerStartTask(*this);
		}

		bool Task::stop()
		{
			__BKPT(0);
			return true;
		}

		void Task::taskFinished()
		{
			__BKPT(0);
		}

		bool __attribute__((naked)) Task::startTaskStub(Task* task)
		{
			asm volatile("PUSH {LR}");
			svc(ServiceCall::SvcNumber::startTask);
			asm volatile(
				"POP {LR}\n\t"
				"BX LR");
		}
}
