#pragma once

#ifdef STM32L432xx

#include "../processor/ST/l4/stm32l432xx.h"

#define VECTOR_TABLE_SIZE (98)
#define VECTOR_TABLE_ALIGNEMENT (128)

#endif 

#ifdef STM32F411xe
#include "../processor/st/F4/stm32f411xe.h"

#define VECTOR_TABLE_SIZE (98)
#define VECTOR_TABLE_ALIGNEMENT (128)

#endif

#ifdef STM32L496xx
#include <yggdrasil/processor/st/l4/stm32l496xx.h>

#define VECTOR_TABLE_SIZE (106)
#define VECTOR_TABLE_ALIGNEMENT (128)

#endif

#ifdef STM32F303x8
#include "../processor/st/f3/stm32f303x8.h"

//82 Interrupts + 15
#define VECTOR_TABLE_SIZE (97)
#define VECTOR_TABLE_ALIGNEMENT (128)

#endif

#ifdef STM32L476xx
#include "../processor/st/l4/stm32l476xx.h"
#define VECTOR_TABLE_SIZE (97)
#define VECTOR_TABLE_ALIGNEMENT (128)

#endif

#ifdef STM32L475xx
#include "../processor/st/l4/stm32l475xx.h"

#define VECTOR_TABLE_SIZE (97)
#define VECTOR_TABLE_ALIGNEMENT (128)
#endif

#ifdef STM32F303xE
#include "../processor/st/f3/stm32f303xe.h"

#define VECTOR_TABLE_SIZE (100)
#define VECTOR_TABLE_ALIGNEMENT (128)
#endif
