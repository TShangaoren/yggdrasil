#pragma once
#include <cstdint>



namespace interface
{

class ISystem
{
public:
	virtual uint32_t getSystemCoreFrequency() = 0;
	virtual uint32_t getPeripheralClock1Frequency() = 0;
	virtual uint32_t getPeripheralClock2Frequency() = 0;

	virtual bool initSystemClock() = 0;
};

}
