#pragma once
#include <cstdint>


namespace interface
{

	template<typename StreamType>
		class IStream
		{
		public:
	
			typedef void(*callback)(StreamType* buffer, uint16_t size, bool eof);
			
			
			//interface to send data througth stream
			virtual bool send(const StreamType* data, uint16_t size, int64_t timeout)
			{
				return (beginSend(data, size) && endSend(timeout));
			}
	
			//interface to send data througth stream, typically starts DMA
			virtual bool beginSend(const StreamType* data, uint16_t size) = 0;
	
			
			//interface to wait for send to finish, typically wait for DMA interrupt
			virtual bool endSend(int64_t timeout) = 0;
	
			
			//function to setup callback for when data was received
			void setupReceiveCallback(callback handler)
			{
				m_receiveCallback = handler;
			}
	
			//interface to receive data 
			virtual void dataReceived(StreamType* data, uint16_t size, bool eof)
			{
				if (m_receiveCallback != nullptr)
					m_receiveCallback(data, size, eof);
			}
	
			//interface to setup receive interface, must be called after each datareceive
			virtual void receive(StreamType* data, uint16_t size, bool eof) = 0;

	
	
		private:
			callback m_receiveCallback;
	
		};
	
}

